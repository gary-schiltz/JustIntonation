/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

/*****************************************************************************
 * Copyright 2016-2017 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                              Open File Dialog
//=============================================================================


#include "filehandler.h"
#include "system/shared.h"
#include "config.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QStandardPaths>

//-----------------------------------------------------------------------------
//                                  Constructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor
///////////////////////////////////////////////////////////////////////////////

FileHandler::FileHandler()
{
    setModuleName("FileHandler");
}


//-----------------------------------------------------------------------------
//                          Open file open dialog
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Open file open dialog
///////////////////////////////////////////////////////////////////////////////

void FileHandler::openMidiFileDialog (int width, int height)
{
    QString path = QStandardPaths::locate(QStandardPaths::DocumentsLocation,"",
                                       QStandardPaths::LocateDirectory);
    if (path.endsWith("/") or path.endsWith("\\")) path.remove(path.size()-1,1);
    LOGSTATUS << "Open file open dialog" << width << height;
    QString fileName = QFileDialog::getOpenFileName(nullptr,
        tr("Please choose a Midi file"), "", tr("Midi files (*.mid *.midi *.MID)"));
    LOGMESSAGE << "Open file" << fileName;
    emit openMidiFile(fileName,true);
}


//-----------------------------------------------------------------------------
//                      Save data (string) to file
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Save data (string) to file
///////////////////////////////////////////////////////////////////////////////


void FileHandler::saveData (QString data1, QString data2, QString data3)
{
    QString path = QStandardPaths::locate(INT_TEMPERAMENT_DIR,"",
                                       QStandardPaths::LocateDirectory);
    if (path.endsWith("/") or path.endsWith("\\")) path.remove(path.size()-1,1);
    LOGSTATUS << "Searching path is:" << path << " Open file-save dialog";
    QString filename = QFileDialog::getSaveFileName(nullptr,tr("Save custom temperament"),
                       path, tr("Temperament files")+" (*.tem);;"+tr("All files") + " (*)");
    if (filename == "") return;
    if (not filename.contains(".")) filename += ".tem";
    LOGMESSAGE << "Saving to file" << filename;
    LOGSTATUS << "Content" << data1;
    QFile file (filename);
    if (not file.open(QIODevice::WriteOnly))
    {
        QMessageBox ::information (nullptr,tr("Could not write to disk."),file.errorString());
        return;
    }
    else
    {
        QTextStream out(&file);
        out << tag << "\n" << data1 << "\n" << data2 << "\n" << data3;
        file.close();
        return;
    }
}


//-----------------------------------------------------------------------------
//                      Load data (string) from file
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Load data (string) from file
///////////////////////////////////////////////////////////////////////////////

void FileHandler::loadData()
{
    QString path = QStandardPaths::locate(INT_TEMPERAMENT_DIR,"",
                                       QStandardPaths::LocateDirectory);
    if (path.endsWith("/") or path.endsWith("\\")) path.remove(path.size()-1,1);
    LOGSTATUS << "Searching path is:" << path;
    QString filename = QFileDialog::getOpenFileName(0,tr("Load custom temperament"),
                       path, tr("Temperament files")+" (*.tem);;"+tr("All files") + " (*)");
    if (filename == "") return;
    if (not filename.contains(".")) filename += ".tem";
    LOGMESSAGE << "Loading from file" << filename;
    QFile file (filename);
    if (not file.open(QIODevice::ReadOnly))
    {
        QMessageBox ::information (0,tr("Could not read from disk."),file.errorString());
        return;
    }
    else
    {
        QTextStream in(&file);
        QString firstline = in.readLine();
        if (firstline != tag)
        {
            QMessageBox ::information (0,tr("Reading Error"),tr("This is not a data file generated by JustIntonation"));
            return;
        }
        else
        {
            QString data1 = in.readLine();
            QString data2 = in.readLine();
            QString data3 = in.readLine();
            LOGSTATUS << "Content: " << data1 << data2 << data3;
            emit loadedData(data1,data2,data3);

        }
    }
}


/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

/*****************************************************************************
 * Copyright 2016-2017 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//             Tuning Log - Create a data log of all tuning events
//=============================================================================

#ifndef LOGFILE_H
#define LOGFILE_H

#include <QMidiMessage>
#include <QMap>
#include <QVariant>
#include <QElapsedTimer>
#include <QFile>
#include <QTextStream>

#include "system/log.h"
#include "system/shared.h"

///////////////////////////////////////////////////////////////////////////////
/// \brief Create a data log of keypress and tuning events
/// \details This class produces a log of selected keypress and tuning events.
///////////////////////////////////////////////////////////////////////////////

class LogFile : public QObject, public Log
{
    Q_OBJECT
public:
    LogFile();

signals:
    /// \details Signal sending a new string to the Qml output text area
    void signalNewLogMessage (QVariant str);

public slots:
    void activateLogFile (bool on);
    void receiveMidiEvent (QMidiMessage event);
    void receiveTuningCorrections(const QMap<int, double> &corrections);

private:
    QString noteName(int key);
    void addLine (quint64 milliseconds, const QString &line);
    void registerKeypress (int key, bool pressed);
    void flushKeypress ();

private:
    QElapsedTimer mTimer;
    bool isActive;
    QList<int> mPressed, mReleased;
    qint64 mTimeOfLastKeypress;
    QString mTuningMessage;
};

#endif // LOGFILE_H

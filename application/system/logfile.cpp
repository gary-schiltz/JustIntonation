/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

/*****************************************************************************
 * Copyright 2016-2017 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                   Create a data log of all tuning events
//=============================================================================

#include "logfile.h"

#include <QTimer>
#include <QTime>

//-----------------------------------------------------------------------------
//                               Constructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor of the LogFile, resets member variables
///////////////////////////////////////////////////////////////////////////////

LogFile::LogFile()
    : mTimer()
    , isActive(false)
    , mPressed()
    , mReleased()
    , mTimeOfLastKeypress(-1)
    , mTuningMessage("")
{
    setModuleName("LogFile");
    mTimer.start();
}


//-----------------------------------------------------------------------------
//                       Switch logging on and off
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Switch logging on and off
/// \param on : true = on, false = off
///////////////////////////////////////////////////////////////////////////////

void LogFile::activateLogFile (bool on)
{

    LOGMESSAGE << "TURNED" << on;
    isActive = on;
}


//-----------------------------------------------------------------------------
//                           Note Name of a key
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Note Name of a key
/// \param key : Number of the key (0..127)
/// \return String with a human-readable name of the corresponding note
///////////////////////////////////////////////////////////////////////////////

QString LogFile::noteName (int key)
{
    const QVector<QString> names =
        {"C","C#","D","D#","E","F","F#","G","G#","A",tr("A#"),tr("B")};
    return names[key%12] + QString::number(key/12-1);
}

//-----------------------------------------------------------------------------
//                      Public slot: Receive MIDI event
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief  Public slot: Receive MIDI event
/// \details This is the first main input slot of the class. It receieves
/// MIDI events indicating whether a key on the keyboard has been pressed
/// or released.
/// \param event : A Midi message containing data about keypresses
///////////////////////////////////////////////////////////////////////////////

void LogFile::receiveMidiEvent(QMidiMessage event)
{
    int key = event.byte1() & 0x7F;

    switch (event.byte0() & 0xF0)
    {
    // Press key: Note on
    case 0x90:
        registerKeypress (key,event.byte2()>0);
        break;

    // Release key: Note off
    case 0x80:
        registerKeypress (key,false);
        break;
    }
}


//-----------------------------------------------------------------------------
//                   Public slot: Receive tuning corrections
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: Receive tuning corrections
/// \details This slot receives the tuning corrections from the tuner and
/// sends them to the log file output. Only the first and the last tuning
/// correction after a keypress or release are sent to the output while
/// all adiabatically equilibrating tuning corrections in between are
/// suppressed in order to keep the output compact.
/// \param corrections : Map containing <key,cent> microtuning pairs
///////////////////////////////////////////////////////////////////////////////

void LogFile::receiveTuningCorrections (const QMap<int, double> &corrections)
{
    if (not isActive) return;
    if (corrections.size() == 0) return;

    qint64 time = mTimer.elapsed();
    if ((mPressed.size()>0 or mReleased.size()>0) and time != mTimeOfLastKeypress)
        flushKeypress();

    QString msg = "<font color=\"orange\">" +
            QString::number(0.01*qRound(100*(corrections.first()))) + "</font> ";
    double prevValue = 0;
    for (auto it = corrections.begin(); it != corrections.end(); it++)
    {
        int key = it.key() % 128;
        if (key != corrections.firstKey())
        {
            msg += " " + QString::number(0.01*qRound(100*(it.value()-prevValue))) +  " ";
            prevValue = it.value();
        }
        msg += "</font>|<b>"+noteName(key)+"</b>|<font color=\"blue\">";
        prevValue = it.value();
    }
    if (mTuningMessage.length()==0) addLine(mTimer.elapsed(),msg);
    mTuningMessage = msg;
}


//-----------------------------------------------------------------------------
//                      Add a line to the logfile output
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Add a line to the logfile output
/// \param milliseconds : The actual time since startup in milliseconds
/// \param line : String containing the text to be sent to the logfile
///////////////////////////////////////////////////////////////////////////////

void LogFile::addLine (quint64 milliseconds, const QString &line)
{
    if (not isActive) return;
    QTime time(0,static_cast<int>(milliseconds/60000L),
               static_cast<int>((milliseconds/1000L)%60L),
               static_cast<int>(milliseconds%1000L));
    emit signalNewLogMessage(time.toString("mm:ss.zzz") + "  <font color=\"blue\">" + line + "</font>");
}


//-----------------------------------------------------------------------------
//                Public slot: Register pressed or released key
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: Register pressed or released key
/// \details This function registers newly pressed and released keys.
/// If there have been suppressed tuning messages before the last one will
/// be sent to the output as well. Multiple keypresses at the same time
/// (in the resolution of milliseconds) will be collected in a single line.
/// When the collection process is finished the function flushKeypress() is
/// called.
/// \param key : Number of the key
/// \param pressed : pressed = true, released = false
///////////////////////////////////////////////////////////////////////////////

void LogFile::registerKeypress(int key, bool pressed)
{
    if (mTuningMessage.length()>0) { addLine(mTimer.elapsed(),mTuningMessage);
        mTuningMessage.clear(); }
    qint64 time = mTimer.elapsed();
    if ((mPressed.size()>0 or mReleased.size()>0) and time != mTimeOfLastKeypress)
        flushKeypress();

    {
        if (pressed) mPressed.append(key); else mReleased.append(key);
        mTimeOfLastKeypress = time;
    }
}


//-----------------------------------------------------------------------------
// Terminate collection of multiple keypresses/releases and send them to the logfile
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Terminate collection of multiple keypresses/releases and send
/// them to the logfile.
///////////////////////////////////////////////////////////////////////////////

void LogFile::flushKeypress()
{
    if (mPressed.empty() and mReleased.empty()) return;
    QString line; QTextStream str(&line);
    if (mReleased.size()>0)
    {
        qSort(mReleased);
        str << "<font color=\"red\">RELEASED";
        for (auto &e : mReleased) str << " - " << noteName(e);
        str << "</font> ";
    }
    if (mPressed.size()>0)
    {
        qSort(mPressed);
        str << "<font color=\"green\">PRESSED";
        for (auto &e : mPressed) str << " - " << noteName(e);
        str << "</font> ";
    }
    addLine(mTimeOfLastKeypress,line);
    mPressed.clear(); mReleased.clear();
}

/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                                Midi handler
//=============================================================================

#ifndef MIDIHANDLER_H
#define MIDIHANDLER_H

#include <QObject>
#include <QMidiMessage>

#include "sampler/soundgenerator.h"
#include "audio/audiooutput.h"
#include "modules/system/threadbase.h"

///////////////////////////////////////////////////////////////////////////////
/// \brief Class handling Midi events
///////////////////////////////////////////////////////////////////////////////

class MidiHandler : public ThreadBase
{
    Q_OBJECT
public:
    MidiHandler(SoundGenerator &soundgenerator, AudioOutput &audiooutput);

public slots:
    void receiveMidiEvent(const QMidiMessage &event);

signals:
    void setQmlTemperamentIndex(QVariant index);
    void changeTargetFrequency (QVariant df);
    void signalSustainPedal (bool pressed);
    void signalSostenutoPedal (bool pressed);
    void signalSoftPedal (bool pressed);

private:
    SoundGenerator *pSoundGenerator;
    AudioOutput *pAudioOutput;
    QVector<quint8> mDrumChannels;
    int mModulationWheel;
    int mPitchBendWheel;
    int mTemperamentIndex;
};

#endif // MIDIHANDLER_H

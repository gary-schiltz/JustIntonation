/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                                  Sampler
//=============================================================================

#ifndef SAMPLER_H
#define SAMPLER_H

#include "audio/audiogenerator.h"
#include "instrument/wave.h"
#include "instrument/voice.h"

class SoundGenerator;
class Tone;
class Tuner;

///////////////////////////////////////////////////////////////////////////////
/// \brief Sound-generating module which plays the tones held by the SoundGenerator.
/// \details This class implements the AudioGenerator from the audio library.
/// It has to be initialized with a pointer to the SoundGenerator which allows
/// the sampler to access the list of tones. The Sampler itself does not
/// hold any local information about the tone being played, all that is
/// contained in the SoundGenerator.
///
/// PULL MODE:
///
/// As implementation of AudioGenerator the sample operates in the pull mode,
/// that is, the generateSound function is called by the QAudioDevice whenever
/// more data is needed. If no tones are queued the sampler simply returns
/// as much zeroes as needed. For this reason the sampler does not provide
/// any start/stop or mute functionality, instead this should be managed
/// directly by the audio device.
/// \note This class contains the most time-critical part for sound generation.
/// It is called by the audio-device in pull mode, i.e., the code is executed
/// in the thread of the audio device.
/// \see SoundGenerator
///////////////////////////////////////////////////////////////////////////////

class Sampler : public AudioGenerator
{
public:
    Sampler();
    void init (SoundGenerator *soundgenerator);
    void exit(){}

private:
    size_t generateSound (char* charbuffer, size_t maxSize) override final;

    SoundGenerator *pSoundGenerator;    ///< Pointer to the SoundGenerator
    QList<Tone> *pTones;                ///< Pointer to the list of tones held by SoundGenerator

    QVector<qint32> mLeft,mRight;       ///< Audio signal to be constructed
};

#endif // SAMPLER_H

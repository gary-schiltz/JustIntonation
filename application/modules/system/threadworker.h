/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                             Thread worker class
//=============================================================================

#ifndef THREADWORKER_H
#define THREADWORKER_H

#include <QThread>

#include "log.h"

class ThreadBase;

///////////////////////////////////////////////////////////////////////////////
/// \brief Helper class for ThreadBase
/// \details ThreadWorker is a helper class for ThreadBase. It is needed because the
/// new thread needs to be started in an instance different from the calling
/// instance so that the slots of the calling instance can be moved later to the
/// thread of the new instance. This trick ensures that all slots of the
/// calling instance (ThreadBase) are running in the newly created thread.
/// (This is the main purpose of the whole construction)
/// \see ThreadBase
///////////////////////////////////////////////////////////////////////////////

class ThreadWorker : public QThread, public Log
{
    Q_OBJECT
public:
    ThreadWorker (ThreadBase *threadbase);

    void setPriority (const QThread::Priority p);
    void setTimerInterval (const int msec, const int firstMsec);
    void setThreadName (const QString name);
    QString getThreadName() const;

    bool start();
    bool stop();

signals:
    /// \brief Helper signal for starting the temporarily existing timer
    /// \details Since we want the timer to be executed in the new thread
    /// its instance is created temporarily from the new thread. Therefore,
    /// the slots of the timer are not always existing. This signal
    /// is simply forwarded to the timer slot.
    void startTimer();
    /// \brief Helper signal for stopping the temporarily existing timer
    /// \details Since we want the timer to be executed in the new thread
    /// its instance is created temporarily from the new thread. Therefore,
    /// the slots of the timer are not always existing. This signal
    /// is simply forwarded to the timer slot.
    void stopTimer();

private:
    virtual void run() override final;
    void setCurrentThreadName(QString threadname);

private:
    ThreadBase *pThreadBase;        ///< Pointer back to the instance of the ThreadBase
    QString mThreadName;            ///< Assigned thread name
    QThread::Priority mPriority;    ///< Assigned priority of the thread
    int mInterval, mFirstInterval;  ///< Assigned periodic waiting time in milliseconds
};

#endif // THREADWORKER_H

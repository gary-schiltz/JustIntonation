/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                  Tuner: Module for adaptive tuning
//=============================================================================

#ifndef TUNER_H
#define TUNER_H

#include <QObject>
#include <QVector>
#include <QMap>
#include <QVariant>
#include <QTimer>
#include <QElapsedTimer>
#include <QMidiMessage>

#include "tunerreadme.h"
#include "system/threadbase.h"
#include "tunerkeydata.h"
#include "tuneralgorithm.h"

///////////////////////////////////////////////////////////////////////////////
/// \brief This is the main class of the tuner module.
/// \ingroup tuner
/// \details This class is basically a wrapper class which supports the
/// TunerAlgorithm and communicates with the outside world via Qt signals
/// and slots. It keeps track of the pressed keys and generates the memory
/// function M(t).
///////////////////////////////////////////////////////////////////////////////

class Tuner : public ThreadBase
{
    Q_OBJECT
public:
    Tuner();                            // Constructor
    bool init() override final;         // Initialization after construction
    bool start() override final;        // Start tuner thread

    //-------------------------------------------------------------------------

public slots:

    void setFrequencyOfA4 (double f=440);
    void setTuningMode (int mode, int wolfsShift);      // 0:ET  1:UT  2:JI
    void enableTuning (bool enable);
    void setStaticTuningMode (bool enable, int reference = 0);
    void setEnvelopeParameters (
         double secondsSustainBass,
         double secondsSustainTreble,
         double secondsRelease);

    void setIntervalSize   (int semitones, double cents);
    void setIntervalWeight (int semitones, double weight);
    void setPitchProgressionCompensationParameter (double lambda);
    void setDelayParameter (double delay);
    void setMemoryLength (double seconds);

    void receiveMidiEvent (QMidiMessage event);
    void resetPitchProgression();

    //-------------------------------------------------------------------------

signals:
    /// \brief Signal telling the GUI that the tuner is ready and that the GUI
    /// may send the user-defined parameters to the tuner.
    void signalReadyForReceivingParameters();

    /// \brief Signal sending the tuning results to the microtonal sound device
    /// in the form of a map keyindex -> cents for all active keys
    void signalTuningCorrections (QMap<int,double> corrections);

    /// \brief Signal that periodically transmits the average pitch progression.
    /// In the GUI this progression is indicated by a circular gauge.
    void signalAveragePitchDrift (double progression);

    /// \brief Signal emitting the tuned interval sizes in a human-readable form
    void signalIntervalString (QVariant str);

    /// \brief Signal emitting the network tension in the tuned chord,
    /// indicating the deviation from just intonation
    void signalTension (QVariant mu);

    //-------------------------------------------------------------------------

private:
    // Constants:
    const uint tuningIntervalMsec = 20;         ///< Update interval tuning

    // Constants for envelope simulation:
    const uint updateIntervalMsec = 20;         ///< Update interval envelope
    const uint noteTimeoutSeconds = 60;
    const double cutoffIntensity = 0.001;
    const double cutoffMemory = 0.0001;
    const double memoryOnSeconds = 0.3;
    const double memoryOnFactor  = exp(-0.005*updateIntervalMsec/memoryOnSeconds);

private:
    // Member variables
    TunerAlgorithm mTunerAlgorithm;             ///< The tuning algorithm
    double mGlobalTargetPitchInCents;           ///< Global pitch against 440Hz
    int mTuningMode;                            ///< Actual tuning mode
    int mStaticReferenceKey;                    ///< Index of static UT reference key
    int mWolfsIntervalShift;                    ///< Placement of the wolfs interval
    bool mTuningEnabled;                        ///< Flag for temporary on/off
    QString mLastMessage;                       ///< remember last message sent
    QVector<double> mIntervalSizes;             ///< List of 12 interval sizes
    QVector<double> mIntervalWeights;           ///< List of 12 interval weights
    QVector<KeyData> mKeyDataVector;            ///< Vector containing all key data
    QElapsedTimer *pElapsedTimer;               ///< Time elapsed since construction
    bool mStaticTuningModeEnabled;              ///< Flag: Tune statically
    double mPitchAutoCorrectionParameter;       ///< Pitch correction parameters
    double mDelayParameter;                     ///< Delay time
    double mMemoryOffFactor;                    ///< Update factor for memory decay
    qint8 mWaitingTimeMsec;                     ///< Waiting time before tuning event

private:
    friend class TunerAlgorithm;

    qint64 getNow();
    void handlePitchDrift();
    void emitPitchCorrections();

    virtual void initiallyCalledWorker() override final;
    virtual void periodicallyCalledWorker() override final;
    virtual void finallyCalledWorker() override final;

private slots:
    void tune();
};

#endif // TUNER_H

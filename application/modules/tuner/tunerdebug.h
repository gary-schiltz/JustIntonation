/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

/*****************************************************************************
 * Copyright 2016-2017 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//         Tuning algorithm debugging tools (normally not included)
//=============================================================================

#ifndef TUNERDEBUG_H
#define TUNERDEBUG_H

QString f(Eigen::MatrixXd mat)
{
    QString str;
    QTextStream ss(&str);
    const int N=static_cast<int>(mat.rows()), M=static_cast<int>(mat.cols());
    for (int n=0; n<N; ++n)
    {
        for (int m=0; m<M; ++m)
        {
            ss << QString::number(mat(n,m));
            if (m<N) ss<<" ";
        }
        ss << "|";
    }
    return str;
}

QString f(Eigen::MatrixXi mat)
{
    QString str;
    QTextStream ss(&str);
    const int N=static_cast<int>(mat.rows()), M=static_cast<int>(mat.cols());
    for (int n=0; n<N; ++n)
    {
        for (int m=0; m<M; ++m)
        {
            ss << QString::number(mat(n,m));
            if (m<N) ss<<" ";
        }
        ss << "|";
    }
    return str;
}

QString f(Eigen::VectorXd vec)
{
    QString str;
    const int N=static_cast<int>(vec.rows());
    QTextStream ss(&str);
    for (int n1=0; n1<N; ++n1)
    {
        ss << QString::number(vec(n1));
        if (n1<N) ss<<" ";
    }
    return str;

}

#endif // TUNERDEBUG_H

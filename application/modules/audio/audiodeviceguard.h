/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                            Audio Device Guard
//=============================================================================

#ifndef AUDIODEVICEGUARD_H
#define AUDIODEVICEGUARD_H

#include <QTimer>
#include <QThread>

#include "audiobase.h"

///////////////////////////////////////////////////////////////////////////////
/// \brief Audio Device Guard
/// \ingroup audio
/// \details The AudioDeviceGuard is a helper class that calls the function
/// updateListOfDevices() of AudioBase periodically from an independent thread.
/// This class is required since requesting a list of devices takes a
/// certain amount of CPU time (up to one second) and would interrupt the
/// audio stream if carried out on the same thread.
///////////////////////////////////////////////////////////////////////////////

class AudioDeviceGuard : public ThreadBase
{
    Q_OBJECT
public:
    AudioDeviceGuard (AudioBase* output=nullptr);
    ~AudioDeviceGuard();
    bool startMonitoring (int seconds=20);

private:
    void periodicallyCalledWorker() override final;

    AudioBase *pAudioBase;          ///< Pointer back to the audio object
};

#endif // AUDIODEVICEGUARD_H

/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                           Audio output device
//=============================================================================

#include "audiooutputdevice.h"

#include "audiooutput.h"
#include "audiooutputdevice.h"

// Note that the default sample rates etc are specified in

//-----------------------------------------------------------------------------
//                              Constructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor, resetting the member variables
/// \param audiooutput : Pointer back to the AudioOutput
///////////////////////////////////////////////////////////////////////////////

AudioOutputDevice::AudioOutputDevice(AudioOutput *audiooutput)
    : pAudioOutput(audiooutput)
    , pQtAudioOutputStream(nullptr)
{
    setModuleName("Audio");
}


//-----------------------------------------------------------------------------
//                      Start the audio output device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Connect and start the audio output device
/// \param parameters : AudioDeviceParameters used for the connection.
/// If the device name is empty the function attempts to connect the
/// system's default audio output device.
/// \return True if device could be opened successfully.
///////////////////////////////////////////////////////////////////////////////

bool AudioOutputDevice::connect(const AudioParameters &parameters)
{
    if (not pAudioOutput) return false;

    // Copy the parameters. The finally realized version may be different
    AudioParameters realizedParameters(parameters);

    // If sample rate is zero set default value
    if (realizedParameters.sampleRate <= 0) realizedParameters.sampleRate = 44100;

    // If channel count is zero set default value
    if (realizedParameters.channelCount <= 0) realizedParameters.channelCount = 2;

    LOGSTATUS<< "Connect audio device: Device name =" << realizedParameters.deviceName <<
                ", Sample rate =" << realizedParameters.sampleRate <<
                ", Sample size =" << realizedParameters.sampleSize <<
                ", Channel count =" << realizedParameters.channelCount <<
                ", Buffer size =" << realizedParameters.bufferSize;

    QAudioDeviceInfo info;
    // If device name is empty look for default device
    if (realizedParameters.deviceName.size()==0)
    {
        info = QAudioDeviceInfo::defaultOutputDevice();
        realizedParameters.deviceName = info.deviceName();
        if (realizedParameters.deviceName.size()==0)
        {
            LOGWARNING << "No default device found";
            return false;
        }
        else LOGSTATUS << "Found default device named" << realizedParameters.deviceName;
    }
    else // if device name is not empty
    {
        // Look up whether the selected device exists
        LOGSTATUS << "Now searching for an audio device named" << realizedParameters.deviceName;
        bool found = false;
        QStringList deviceList; // several devices may be compatible with the name
        QList<QAudioDeviceInfo> availableDevices(QAudioDeviceInfo::availableDevices(QAudio::AudioOutput));
        for (const QAudioDeviceInfo &deviceinfo : availableDevices)
        {
            if (deviceinfo.deviceName() == realizedParameters.deviceName)
            {
                info = deviceinfo;
                found = true;
                deviceList << deviceinfo.deviceName();
                break;
            }
        }
        if (not found)
        {
            LOGWARNING << "Could not find the audio device" << realizedParameters.deviceName
                    << "in the list" << deviceList;
            LOGWARNING << "Now Trying to connect to default device";
            LOGSTATUS << "Recursive call of connect()";
            realizedParameters.deviceName="";
            return connect(realizedParameters);
        }
        else
        {
            LOGSTATUS << "Found available audio device" << realizedParameters.deviceName;
        }
    }
    LOGMESSAGE << "Trying to open audio device" << realizedParameters.deviceName;
    LOGMESSAGE << "Tentative parameter set: samplerate =" << realizedParameters.sampleRate
            << "samplesize =" << realizedParameters.sampleSize
            << "channels =" << realizedParameters.channelCount
            << "buffersize =" << realizedParameters.bufferSize;

    // Format specification:
    QAudioFormat format;
    format.setSampleRate(realizedParameters.sampleRate);
    format.setChannelCount(realizedParameters.channelCount);
    format.setCodec("audio/pcm");
    format.setSampleSize(16);
    format.setByteOrder(QAudioFormat::LittleEndian);
    format.setSampleType(QAudioFormat::SignedInt);

    // Look up whether the format is supported
    if (not info.isFormatSupported(format))
    {
        LOGWARNING << "Audio format is not supported by backend,"
                << "falling back to nearest format";
        format = info.nearestFormat(format);
        if (not info.isFormatSupported(format))
        {
            LOGWARNING << "Fallback failed. Probably there is"
                    << "no audio output device available.";
            return false;
        }
    }

    LOGMESSAGE << "Format sample size, type and rate =" << format.sampleSize()
            << format.sampleType() << format.sampleRate();
    if ((format.sampleSize()!=16 and format.sampleSize()!=24)
            or format.sampleType() != QAudioFormat::SignedInt)
    {
        LOGWARNING << "Sample type and/or size are not supported by the application.";
        return false;
    }

    // If the wanted sample rate cannot be met reset to the suggested values:
    if (format.sampleRate() != realizedParameters.sampleRate)
    {
        LOGMESSAGE << "Resetting sample rate to " << format.sampleRate();
        realizedParameters.sampleRate = format.sampleRate();
    }

    // If the wanted channel count cannot be met reset to the suggested values:
    if (format.channelCount() != realizedParameters.channelCount)
    {
        LOGMESSAGE << "Resetting channel count to " << format.channelCount();
        realizedParameters.channelCount = format.channelCount();
    }

    mSampleSize = format.sampleSize();

    // ############### CREATE QT AUDIO OUTPUT STREAM ##################

    pQtAudioOutputStream = new QAudioOutput(info, format);

    // set volume to 100%
    pQtAudioOutputStream->setVolume(1);

    // Set audio buffer size
    if (realizedParameters.bufferSize > 0)
    {
        LOGMESSAGE << "Set Qt audio buffer size to" << realizedParameters.bufferSize;
        pQtAudioOutputStream->setBufferSize(realizedParameters.bufferSize);
    }

    // If error detected terminate
    if (pQtAudioOutputStream->error() != QAudio::NoError)
    {
        LOGWARNING << "Error opening QAudioOutput with error:" << pQtAudioOutputStream->error();
        return false;
    }


    // ################ OPEN QT AUDIO OUTPUT STREAM ##################
    if (not open(QIODevice::ReadOnly))
    {
        LOGWARNING << "QAudioOutput could not be opened, aborting.";
        return false;
    }

    // ################ START QT AUDIO OUTPUT STREAM ##################
    pQtAudioOutputStream->start(this);
    if (pQtAudioOutputStream->error() != QAudio::NoError)
    {
        LOGWARNING << "Error starting QAudioOutput with error ",pQtAudioOutputStream->error();
        return false;
    }

    // This should not happen
    if (realizedParameters.deviceName != info.deviceName())
    {
        LOGERROR << "Severe consistency error";
        return false;
    }

    // Update current device name
    LOGMESSAGE << "Opened audio output device:" << realizedParameters.deviceName;
    realizedParameters.supportedSampleRates = info.supportedSampleRates();
    realizedParameters.active = true;
    pAudioOutput->setActualParameters(realizedParameters);
    return true;
}


//-----------------------------------------------------------------------------
//                         Stop the audio output device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Stop the audio output device
///////////////////////////////////////////////////////////////////////////////

void AudioOutputDevice::disconnect()
{
    if (pQtAudioOutputStream)
    {
        pQtAudioOutputStream->stop();
        delete pQtAudioOutputStream;
        pQtAudioOutputStream = nullptr;
    }
}


//-----------------------------------------------------------------------------
//                            Set global volume
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Set the global volume
/// \param volume : Volume level between 0 and 1
///////////////////////////////////////////////////////////////////////////////

void AudioOutputDevice::setVolume(double volume)
{
    if (pQtAudioOutputStream) pQtAudioOutputStream->setVolume(volume);
}


//-----------------------------------------------------------------------------
//                              Read PCM data
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Read PCM data
/// \details This is a virtual function defined in QIODevice which is now
/// implemented here. The call is redirected to the SoundGenerator.
/// \param data : Pointer to the PCM data
/// \param maxSize : Maximal size to be generated in bytes
/// \return : Actual number of bytes that have been generated
///////////////////////////////////////////////////////////////////////////////

qint64 AudioOutputDevice::readData (char* data, qint64 maxSize)
{
    return static_cast<qint64>(
       pAudioOutput->getAudioGenerator()->generateSound(data,(size_t)maxSize));
}


//-----------------------------------------------------------------------------
//                     Empty implementation for writing
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Empty implementation for writing without functionality
/// \details This function is virtual in QIODevice and has to be implemented,
/// even if it does nothing.
/// \param data
/// \param maxSize
/// \return : Zero
///////////////////////////////////////////////////////////////////////////////

qint64 AudioOutputDevice::writeData (const char * data, qint64 maxSize)
{ (void)data; (void)maxSize; return 0; }


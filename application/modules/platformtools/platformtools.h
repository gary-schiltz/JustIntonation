/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#ifndef PLATFORMTOOLS_H
#define PLATFORMTOOLS_H

#include <memory>
#include <assert.h>

#include <QStorageInfo>

class Application;

///////////////////////////////////////////////////////////////////////////////
/// \brief Base class providing support for various platforms
/// \details This singleton class is used to access platform-dependent
/// components. The platform-dependent components are defined as derived
/// casses named XXXPlatformTools. To create an instance of these tools
/// one has to instantiate a global object of the type
/// \code PlatformToolsInstanciator<XXXPlatformTools> instance;
/// \endcode
///////////////////////////////////////////////////////////////////////////////

class PlatformTools
{
protected:
    PlatformTools() {}

public:

    ///////////////////////////////////////////////////////////////////////////
    /// \brief
    /// \details To access platform-dependent functions declare them as virtual
    /// functions here in this base, override them in the corresponding
    /// platform-dependent derived class and access them by calling
    /// \code PlatformTools::getSingleton().<functionname>(<arguments>);
    /// \endcode
    ///////////////////////////////////////////////////////////////////////////

    static PlatformTools &getSingleton()
    {
        assert(getPointerToInstance());
        return *getPointerToInstance();
    }

    ///////////////////////////////////////////////////////////////////////////
    /// \brief Platform-dependent initialization
    ///////////////////////////////////////////////////////////////////////////

    virtual void init (Application &application) {(void)application;}


    ///////////////////////////////////////////////////////////////////////////
    /// \brief Function to disable the screen saver.
    ///////////////////////////////////////////////////////////////////////////

    virtual void disableScreensaver() {}


    ///////////////////////////////////////////////////////////////////////////
    /// \brief Function to enable the screen saver.
    ///////////////////////////////////////////////////////////////////////////

    virtual void enableScreensaver() {}


    //-------------------------------------------------------------------------
    //                        Determine free disk space
    //-------------------------------------------------------------------------
    ///////////////////////////////////////////////////////////////////////////
    /// \brief  Determine free disk space
    /// \param path : Path of the location for which the free disk space
    /// has to be determined
    /// \return : Free space as a qint64 in bytes.
    ///////////////////////////////////////////////////////////////////////////

    virtual qint64 getFreeDiskSpace(const QString &path)
    {
        return QStorageInfo(path).bytesTotal();
    }


//=================================================================================

public:

    ///////////////////////////////////////////////////////////////////////////
    /// \brief Function holding the instance pointer as a static unique pointer
    ///////////////////////////////////////////////////////////////////////////

    static std::unique_ptr<PlatformTools> &getPointerToInstance()
    {
        static std::unique_ptr<PlatformTools> mInstance(new PlatformTools);
        return mInstance;
    }
};

///////////////////////////////////////////////////////////////////////////////
/// \brief Helper class for instantiating objects derived from PlatformTools
///////////////////////////////////////////////////////////////////////////////

template <class T>
class PlatformToolsInstanciator {
public:
    friend class PlatformTools;
    PlatformToolsInstanciator()
    {
        PlatformTools::getPointerToInstance().reset(new T());
    }
};

#endif // PLATFORMTOOLS_H

/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                        Microtonal Midi Helper Class
//=============================================================================

#ifndef MICROTONALHELPER_H
#define MICROTONALHELPER_H

#include "system/log.h"

class MidiMicrotonal;

///////////////////////////////////////////////////////////////////////////////
/// \ingroup microtonal
/// \brief Auxiliary class for microtonal conversion
///////////////////////////////////////////////////////////////////////////////

class MidiMicrotonalHelper : public QObject, public Log
{
    Q_OBJECT
public:
    MidiMicrotonalHelper(MidiMicrotonal *midimicrotone);

    void setInstrument (int instrument);
    void turnNoteOn (int channel, int key, int volume, double delta);
    void turnNoteOff (int channel, int key, double delta);
    void afterTouch (int channel, int key, int volume, double delta);
    void controlChange (int channel, int control, int value, double delta);
    void bankSelect (int bank, double delta);
    void allNotesOff();
    void resetAllControllers();
    void localControl (bool on);
    void tune (int key, double pitch);

public slots:
    void sendInitialMidiCommands (void);

private:
    void playNote (bool on, int mappedChannel, int key, int vol, double delta);
    void setPitchBend (int channel, double pitch);

private:
    MidiMicrotonal *pMidiMicrotonal;                ///< Pointer back
    QVector<bool> mMappedChannelIsUsed;             ///< Indicates usage of channel
    QVector<int> mKeyOfMappedChannel;               ///< Associated key
    int mSequentialNumber;                          ///< Increasing tag
    QVector<int> mSequentialNumberOfMappedChannel;  ///< Tag storage
    QVector<double> mCurrentPitch;                  ///< Pitch
    int mInstrument;                                ///< The selected instrument
    const int cDrumChannel = 9;                     ///< Take out drum channel
};

#endif // MICROTONALHELPER_H

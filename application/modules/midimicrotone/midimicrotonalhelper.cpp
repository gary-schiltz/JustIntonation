/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                        Microtonal Midi Helper Class
//=============================================================================

#include "midimicrotonalhelper.h"
#include "midimicrotonal.h"


//-----------------------------------------------------------------------------
//                               Constructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor, resetting the member variables
/// \param midimicrotone : Pointer back to the instance of MidiMicrotone
///////////////////////////////////////////////////////////////////////////////

MidiMicrotonalHelper::MidiMicrotonalHelper(MidiMicrotonal *midimicrotone)
    : pMidiMicrotonal(midimicrotone)
    , mMappedChannelIsUsed(16,false)
    , mKeyOfMappedChannel(16,-1)
    , mSequentialNumber(0)
    , mSequentialNumberOfMappedChannel(16,0)
    , mCurrentPitch(128,0)
    , mInstrument(0)
{
    setModuleName("MidiMicrotone");
}


//-----------------------------------------------------------------------------
//             Set the instrument (program) of a given input channel
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Set the instrument (program)
/// \param instrument : Midi instrument number (0...127)
///////////////////////////////////////////////////////////////////////////////

void MidiMicrotonalHelper::setInstrument (int instrument)
{
    LOGMESSAGE << "Set Midi output channel to" << instrument;
    mInstrument = instrument & 0x7f;
}


//-----------------------------------------------------------------------------
//                              Turn a note on
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Turn note on
/// \param channel
/// \param key
/// \param volume
/// \param delta
///////////////////////////////////////////////////////////////////////////////

void MidiMicrotonalHelper::turnNoteOn (int channel, int key, int volume, double delta)
{
    if (volume==0)
    {
        turnNoteOff (channel,key,delta);
        return;
    }

    LOGSTATUS << "Turn note on: Channel =" << channel << "/ key =" << key
              << "/ volume =" << volume;

    // Look for oldest channel not in use, called noteChannel
    int mappedChannel = -1;
    int lowestSequentialNumber = 0x7fffffff;
    for (int c=0; c<16; ++c) if (c != cDrumChannel and not mMappedChannelIsUsed[c])
    {
        if (mSequentialNumberOfMappedChannel[c] < lowestSequentialNumber)
        {
            lowestSequentialNumber = mSequentialNumberOfMappedChannel[c];
            mappedChannel = c;
        }
    }

    // If all channels are used search the one with the lowest sequential number
    if (mappedChannel < 0)
    {
        lowestSequentialNumber = 0x7fffffff;
        for (int c=0; c<16; ++c) if (c != cDrumChannel)
        {
            if (mSequentialNumberOfMappedChannel[c] < lowestSequentialNumber)
            {
                lowestSequentialNumber = mSequentialNumberOfMappedChannel[c];
                mappedChannel = c;
            }
        }
        if (mappedChannel < 0) LOGERROR << "Search inconsistent";

        LOGSTATUS << "Release key on mapped channel" << mappedChannel;
        playNote(false,mappedChannel,mKeyOfMappedChannel[mappedChannel],0,0);
    }

    // Now we have a free channel, the noteChannel
    mMappedChannelIsUsed[mappedChannel] = true;
    mKeyOfMappedChannel[mappedChannel] = key;
    mSequentialNumberOfMappedChannel[mappedChannel] = ++mSequentialNumber;
    playNote (true,mappedChannel,key,volume,delta);
}



//-----------------------------------------------------------------------------
//                   Polyphonic Key Pressure (Aftertouch).
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Polyphonic Key Pressure (Aftertouch).
/// \param channel
/// \param key
/// \param volume
/// \param delta
///////////////////////////////////////////////////////////////////////////////

void MidiMicrotonalHelper::afterTouch (int channel, int key, int volume, double delta)
{
    LOGSTATUS << "Polyphonic Key Pressure, channel =" << channel << "/ key =" << key;

    for (int c=0; c<16; ++c) if (c != cDrumChannel and
                                 mMappedChannelIsUsed[c] and
                                 mKeyOfMappedChannel[c] == key)
    {
        quint8 command = 0xA0 | c;
        emit pMidiMicrotonal->sendMidiEvent(QMidiMessage(command, key & 0x7f, volume & 0x7f, delta));
    }
}



//-----------------------------------------------------------------------------
//                   Polyphonic Key Pressure (Aftertouch).
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Polyphonic Key Pressure (Aftertouch).
/// \param channel
/// \param control
/// \param value
/// \param delta
///////////////////////////////////////////////////////////////////////////////

void MidiMicrotonalHelper::controlChange (int channel, int control, int value, double delta)
{
    LOGSTATUS << "Control change, channel =" << channel << "/ control =" << control;

    for (int c=0; c<16; ++c) if (c != cDrumChannel)
    {
        quint8 command = 0xB0 | c;
        emit pMidiMicrotonal->sendMidiEvent(QMidiMessage(command, control & 0x7f, value & 0x7f, delta));
    }
}



//-----------------------------------------------------------------------------
//                                Bank select
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief MidiMicrotonalHelper::bankSelect
/// \param bank
/// \param delta
///////////////////////////////////////////////////////////////////////////////

void MidiMicrotonalHelper::bankSelect(int bank, double delta)
{
    LOGSTATUS << "bank select, bank =" << bank;

    for (int c=0; c<16; ++c) if (c != cDrumChannel)
    {
        quint8 command = 0xB0 | c;
        emit pMidiMicrotonal->sendMidiEvent(QMidiMessage(command, 0, 0, delta));
        command = 0xB0 | c;
        emit pMidiMicrotonal->sendMidiEvent(QMidiMessage(command, 0x20, bank, delta));
        command = 0xC0 | c;
        emit pMidiMicrotonal->sendMidiEvent(QMidiMessage(command, mInstrument & 0x7f, delta));
    }

}


//-----------------------------------------------------------------------------
//                              Turn a note off
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Turn note off
/// \param channel
/// \param key
/// \param delta
///////////////////////////////////////////////////////////////////////////////

void MidiMicrotonalHelper::turnNoteOff (int channel, int key, double delta)
{
    LOGSTATUS << "Turn note off: channel =" << channel << "/ key =" << key;

    for (int c=0; c<16; ++c) if (c != cDrumChannel and
                                 mMappedChannelIsUsed[c] and
                                 mKeyOfMappedChannel[c] == key)
    {
        playNote(false,c,key,0,delta);
        mMappedChannelIsUsed[c] = false;
    }
}


//-----------------------------------------------------------------------------
//                              Turn a note off
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Turn all notes off
///////////////////////////////////////////////////////////////////////////////

void MidiMicrotonalHelper::allNotesOff()
{
    // Send 'all notes off' on all Midi channels
    for (quint8 command = 0xB0U; command < 0xC0U; command++)
        emit pMidiMicrotonal->sendMidiEvent (QMidiMessage(command, 123, 0, 0.0));
    resetAllControllers();
}


//-----------------------------------------------------------------------------
//                           Reset all controllers
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Reset all controllers
///////////////////////////////////////////////////////////////////////////////

void MidiMicrotonalHelper::resetAllControllers()
{
    // Send 'reset all controllers' on all Midi channels
    for (quint8 command = 0xB0U; command < 0xC0U; command++)
        emit pMidiMicrotonal->sendMidiEvent (QMidiMessage(command, 121, 0, 0.0));
}


//-----------------------------------------------------------------------------
//                              Tune a note
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief MidiMicrotonalHelper::tune
/// \details This function receives the key number and the corresponding cent
/// deviatin from the tuner module. It scans the array to look up which of
/// the 16 channels currently plays this key. If so, the pitch bend function
/// is called.
/// \param key : Number of the key
/// \param pitch : pitch versus ET in cent
///////////////////////////////////////////////////////////////////////////////

void MidiMicrotonalHelper::tune (int key, double pitch)
{
    //LOGSTATUS << "**************************" << key << pitch;
    mCurrentPitch[key]=pitch;
    for (int c=0; c<16; c++)
        if (mMappedChannelIsUsed[c] and mKeyOfMappedChannel[c] == key)
        {
            setPitchBend(c,pitch);
        }
}


//-----------------------------------------------------------------------------
//                               Play a note
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////

void MidiMicrotonalHelper::playNote (bool on, int mappedChannel, int key, int vol, double delta)
{
    int command = (on ? 0x90 : 0x80) + (mappedChannel & 0x0f);
    emit pMidiMicrotonal->sendMidiEvent(QMidiMessage(command, key & 0x7f, vol & 0x7f, delta));
    if (on) setPitchBend(mappedChannel,mCurrentPitch[key]);
}



//-----------------------------------------------------------------------------
//                              Set pitch bend
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////

void MidiMicrotonalHelper::setPitchBend (int channel, double pitch)
{
    LOGSTATUS << "Pitch bend: channel =" << channel << "/ pitch =" << pitch;
    if (qAbs(pitch) >= 200)
    {
        LOGWARNING << "Cannot tune" << pitch << "cents";
        return;
    }
    int command = 0xe0 + (channel & 0x0f);
    int pitchbend = qRound(8192 * (1+pitch/200));
    qint8 arg1 = pitchbend & 0x7f;
    qint8 arg2 = (pitchbend >> 7) & 0x7f;
    emit pMidiMicrotonal->sendMidiEvent(QMidiMessage(command,arg1,arg2,0.0));
}


//-----------------------------------------------------------------------------
//                         Send initial Midi commands
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Send initial Midi commands
/// \details This function sends the intial MIDI commands for initialization.
/// In particular it resets all channels to the same mInstrument.
/// Moreover, it switches the local control of the target device off.
///////////////////////////////////////////////////////////////////////////////

void MidiMicrotonalHelper::sendInitialMidiCommands()
{    
    if (not pMidiMicrotonal->isWorking()) return;
    emit pMidiMicrotonal->sendMidiEvent(pMidiMicrotonal->cLoopMarker);

    localControl(false); // Switch off local control
    bankSelect(2,0);
    for (int c=0; c<16; ++c) if (c != cDrumChannel)
        emit pMidiMicrotonal->sendMidiEvent(
                QMidiMessage(0xc0 | c, mInstrument & 0x7f, 0.0));
}


//-----------------------------------------------------------------------------
//                           Enable local control
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Enable local control
/// \details If one plays on an electronic keyboard it is per default
/// controlled locally, that is, pressing a key produces a sound. This means
/// that the keyboard unit and the sound-generating unit in the keyboard are
/// connected. For playing in just intonation, we have to switch the local
/// control off and send all data through the attached computer.
/// \param on : True = local control, false = no local control
///////////////////////////////////////////////////////////////////////////////

void MidiMicrotonalHelper::localControl(bool on)
{
    LOGMESSAGE << "Local control of external Midi =" << on;
    for (quint8 command = 0xB0U; command < 0xC0U; command++)
    {
        emit pMidiMicrotonal->sendMidiEvent (
                    QMidiMessage(command, 122, on ? 0x7f : 0, 0.0));
    }
}

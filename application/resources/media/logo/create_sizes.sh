convert Logo_V1_final.png -resize 32x32 logo1_32x32.png
convert Logo_V1_final.png -resize 48x48 logo1_48x48.png
convert Logo_V1_final.png -resize 64x64 logo1_64x64.png
convert Logo_V1_final.png -resize 128x128 logo1_128x128.png
convert Logo_V1_final.png -resize 256x256 logo1_256x256.png

convert Logo_V2_final.png -resize 32x32 logo2_32x32.png
convert Logo_V2_final.png -resize 64x64 logo2_64x64.png
convert Logo_V2_final.png -resize 128x128 logo2_128x128.png
convert Logo_V2_final.png -resize 256x256 logo2_256x256.png

convert logoframed_256x256.png -resize 32x32 logoframed_32x32.png
convert logoframed_256x256.png -resize 64x64 logoframed_64x64.png
convert logoframed_256x256.png -resize 128x128 logoframed_128x128.png

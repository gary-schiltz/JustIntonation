/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.0
import QtQuick.Controls.Styles 1.4

// Shows a progress bar for loading the instruments

Item {
    property double size: landscape ? 0.6 : 0.9
    property double rad: size*parent.width*0.02
    property double progressfontsize: rad*3
    //anchors.fill: parent

    Header {
        id: header
        anchors.left: parent.left
        anchors.right: parent.right
        height: scalefactor*0.3
    }

    Item {
        anchors.fill: parent
        anchors.leftMargin: parent.width * (1-size)/2
        anchors.rightMargin: parent.width * (1-size)/2
        anchors.topMargin: parent.height/2 + header.height/2 - parent.width*size*0.23
        anchors.bottomMargin: parent.height/2 - header.height/2 - parent.width*size*0.23



        Image {
            id: rectangle2
            anchors.fill: parent
            source: "images/plate.png"
        }

        XText {
            color: "white"
            style: Text.Raised; styleColor: "black"
            anchors.top: parent.top
            anchors.topMargin: parent.height*0.07
            horizontalAlignment: Text.AlignHCenter
            width: parent.width
            font.pixelSize: progressfontsize
            text: window.progressTitle
            spacingFactor: 0.8
        }

        XText {
            color: "white"
            style: Text.Raised; styleColor: "black"
            anchors.top: parent.top
            anchors.topMargin: parent.height*0.34
            horizontalAlignment: Text.AlignHCenter
            width: parent.width
            font.pixelSize: progressfontsize * 0.8
            text: window.progressSubtitle
        }

        Rectangle {
            anchors.top: parent.top
            anchors.topMargin: parent.height*0.51
            anchors.right: parent.right
            anchors.rightMargin: parent.width*0.06
            width: parent.width*0.9
            height: parent.height*0.15
            id: rectangle3

            radius: rad
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#ffffff"
                }

                GradientStop {
                    position: 1
                    color: "#000000"
                }
            }

            ProgressBar {
                id: progressBar1
                anchors.margins: rectangle3.height*0.1
                anchors.fill: parent
                value: window.progressFraction

                style: ProgressBarStyle {
                     background: Rectangle {
                         radius: rad
                         color: "lightgray"
                         border.color: "gray"
                         border.width: 1
                         implicitWidth: 200
                         implicitHeight: 24
                     }
                     progress: Rectangle {
                         radius: rad
                         color: "darkred"
                         opacity: 0.5
                     }
                 }

            }

        }
        XButton {
            anchors.top: parent.top
            anchors.topMargin: parent.height*0.7
            id: cancelButton
            anchors.horizontalCenter: parent.horizontalCenter
            height: parent.height*0.2
            width: parent.height*0.7
            text: qsTr("Cancel")
            onClicked: window.progressCancelButton()
        }
    }
}

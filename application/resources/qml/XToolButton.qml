/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0

ToolButton{
    id: xtoolbutton
    property alias source: img.source
    property int radius: height/2
    property bool highlighted: false
    property bool activated: false
    property bool boundary: true
    implicitWidth: parent.height
    implicitHeight: parent.height
    Image {
        id: img
        asynchronous: true
        smooth: true
        mipmap: true
        anchors.fill: parent
        anchors.margins: pressed ? 0 : parent.height*0.05
    }
    Item {
        id: buttonArea
        visible: false
        anchors.fill: parent
        Rectangle {
            anchors.fill: parent
            anchors.margins: parent.height*0.05
            color: "red"
            radius: height/2
        }
    }

    style: ButtonStyle {
        background: Item {
            implicitWidth: 100
            implicitHeight: 25
            Rectangle {
                id: background
                anchors.fill:parent;
                anchors.margins: parent.height*0.02
                //opacity: activated |highlighted ? 1 : 0.6
                //visible: hovered | highlighted | activated
                visible: false
                radius: xtoolbutton.radius
                gradient: Gradient {
                    GradientStop { position: 0 ; color: "#fff" }
                    GradientStop { position: 1 ; color: "#000" }
                }
            }
            OpacityMask {
                 anchors.fill: background
                 source: background
                 maskSource: buttonArea
                 invert: true
                 visible: boundary
             }
            Rectangle {
                anchors.fill:parent;
                border.width: control.activeFocus ? 2 : 1
                color: highlighted ? "green" : "white"
                border.color: "white"
                opacity: activated |highlighted ? 1 : 0.6
                visible: hovered | highlighted | activated
                radius: xtoolbutton.radius
            }


        }
    }
}

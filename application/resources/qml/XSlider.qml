/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Slider {
    height: 50
    id: slider
    //property int along: orientation===Qt.Vertical ? height : width
    property int perpendicular: orientation===Qt.Vertical ? width : height
    property int digits: 1
    property int factor: 1
    property string unit: ""
    property bool valueVisible: false
    style: SliderStyle {
        handle: Rectangle {
            id: xhandle
            visible: enabled
            height: perpendicular*1.3
            width: height
            radius: height/2
            border.color: "#333"
            color: "lightgray"
            XText {
                visible: valueVisible
                anchors.top: xhandle.bottom
                anchors.topMargin: -xhandle.height*0.2
                font.pixelSize: xhandle.height*0.8
                anchors.horizontalCenter: xhandle.horizontalCenter
                text: (factor * slider.value).toFixed(digits) + unit
            }
        }

        groove: Item {
            implicitHeight: perpendicular*0.4
            implicitWidth: 100
            Rectangle {
                anchors.fill: parent
                radius: height*0.55
                anchors.margins: -parent.height*0.2
                gradient: Gradient {
                    GradientStop { position: 0 ; color: "#fff" }
                    GradientStop { position: 1 ; color: "#000" }
                }
            }
            Rectangle {
                anchors.fill: parent
                radius: height/2
                border.color: "#333"
                color: "lightgray"
            }
            Rectangle {
                visible: slider.enabled
                height: parent.height
                width: styleData.handlePosition
                implicitHeight: perpendicular*0.4
                implicitWidth: 100
                radius: height/2
                color: "darkred"
            }
        }
    }
}

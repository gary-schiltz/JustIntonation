/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                             Just Intonation
//                              Main QML file
//=============================================================================

import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.2
// Import translation module:
import JustIntonation 1.0


// The QML layer and its connection with C++ is organized as follows:
// - The only permanently instantiated QML object is Main (this file)
// - All setting variables are part of Main
// - C++ interacts with Main exclusively via signals and slots
// - Main owns the NavigationManager, which is an instance of StackView
// - StackView calls other pages which are instantiated only temporarily
// - The design of standard QML elements is changed in files beginning with X..

ApplicationWindow {
    id: window
    width: 740
    height: 680
    minimumHeight: 100
    minimumWidth: 100
    title: "Just Intonation"
    visible: true

    //------------------------- Internet URLs ---------------------------------

    readonly property string gitlabUrl: "http://sourcecode.just-intonation.org"
    readonly property string licenseUrl: qsTr("https://www.gnu.org/licenses/gpl.html")
    readonly property string doxygenUrl: "http://doxygen.just-intonation.org"
    readonly property string docsUrl: "http://download.just-intonation.org/Resources/Public/Documentation"
    readonly property string homepageUrl: qsTr("http://www.just-intonation.org")

    //----------------------------- Settings ----------------------------------

    Settings {
        id: settings
        property alias x: window.x
        property alias y: window.y
        property alias width: window.width
        property alias height: window.height
        property alias showWelcome: window.showWelcome
        property alias firstStart: window.firstStart
        property alias temperamentIndex: window.temperamentIndex
        property alias subminorSeventh: window.subminorSeventh
        //The following does not work because of a bug in Qt 5.7,see below
        //see https://bugreports.qt.io/browse/QTBUG-45316
        //property alias customCents: window.customCents
        //property alias weights: window.weights
        property string customCentsString: initialCents.map(String).join("|")
        property string weightString: initialWeights.map(String).join("|")
        //property alias driftCorrectionParameter: window.driftCorrectionParameter
        property alias targetFrequency: window.targetFrequency
        property alias staticTuningOffset: window.staticTuningOffset
        property alias customTemperamentName: window.customTemperamentName
        property alias allowDownload: window.allowDownload
        property alias midiInputAutomaticMode: window.midiInputAutomaticMode
        property alias selectedMidiInputDevice: window.selectedMidiInputDevice
        property alias midiOutputAutomaticMode: window.midiOutputAutomaticMode
        property alias selectedMidiOutputDevice: window.selectedMidiOutputDevice
        property alias currentAudioDevice: window.currentAudioDevice
        property alias externalMidiOutputChannel: window.externalMidiOutputChannel
    }

    // Array bug dirty workaround, see https://bugreports.qt.io/browse/QTBUG-45316
    Component.onCompleted: {
        weights = settings.weightString.split("|").map(Number)
        customCents = settings.customCentsString.split("|").map(Number)
    }
    Component.onDestruction: {
        settings.weightString = weights.map(String).join("|")
        settings.customCentsString = customCents.map(String).join("|")
    }
    // End of workaround

    //---------------------- Reset settings to default ------------------------

    function resetSettings() {
        console.log("Reset all settings")
        resetAudioParameters()
        showWelcome = true
        firstStart = true
        temperamentIndex = 1
        subminorSeventh = false
        customCents = initialCents
        weights = initialWeights
        tuningDelay = 0
        staticTuningOffset = 0
        customTemperamentName = ""
        memoryLength = 3
        driftCorrectionParameter = 0.3
        targetFrequency = 440
        allowDownload = true
        downloading = false
        downloadingProgress = 0
        midiInputAutomaticMode = true
        selectedMidiInputDevice = ""
        midiOutputAutomaticMode = true
        selectedMidiOutputDevice = ""
        currentAudioDevice = ""
        externalMidiOutputChannel = 0
        selectInstrument(0)
    }

    signal resetAudioParameters();
    signal playStartupSound();

    //========== PROPERTIES, SIGNALS AND SLOTS INTERACTING WITH C++ ===========

    //----------------------- Global properties -------------------------------

    // Global properties
    readonly property bool mobile:
        Qt.platform.os=="android" || Qt.platform.os=="ios" || Qt.platform.os=="winphone"
    readonly property bool landscape: width > height
    readonly property int scalefactor: 1/Math.sqrt(1/height/height+1/width/width)
    readonly property int fontsize: scalefactor * 0.08
    readonly property int radius: scalefactor * 0.04
    readonly property string font: mainFont.name

    //---------- Load free font which looks the same on all devices -----------

    FontLoader {
       id: mainFont
       source: "fonts/OpenSans-CondLight.ttf"
    }

    //----------------------------- Translator --------------------------------

    Translator {
        id: translator
        onIndexChanged: updateInstruments()
    }


    //-------------------------- Welcome page ---------------------------------

    property bool showWelcome: true
    property bool firstStart: true

    //-----------------  Download manual, language-dependent ------------------

    function downloadManual() {
        Qt.openUrlExternally(docsUrl + "/" + qsTr("manual_en.pdf"))
    }

    //------------------------------- Log file --------------------------------

    property string newLogMessage: ""
    function appendLogfile (line) { newLogMessage = line; }
    function clearLogFile (clr) { if (clr) newLogMessage = ""; }
    signal activateLogFile (bool on)

    //-------------------------- Instrument selector --------------------------

    // The settings concerning the instruments are managed on the C++ level
    property variant instrumentmodel: []
    property int instrumentindex: 0
    function setQmlInstrumentNames (list) { instrumentmodel = list; }
    function setQmlSelectedInstrument (index) { instrumentindex = index; }
    signal selectInstrument (int index)
    signal updateInstruments()

    //---------------------------- Example selector ---------------------------

    // The settings concerning the instruments are managed on the C++ level
    property int exampleindex: 0
    readonly property int numberOfExamples: 12
    function setQmlSelectedExample (index) { exampleindex = index; }
    function browseExamples (increase) { exampleindex =
             (exampleindex + numberOfExamples + increase)% numberOfExamples;
             window.hearExample(exampleindex+1)
    }

    //----------------------------- Progress bar ------------------------------

    property string progressTitle: "Please wait"
    property string progressSubtitle: ""
    property double progressFraction
    function showProgressBar (str) { progressTitle = str; progressFraction=0;
             navigationmanager.push(navigationmanager.progressIndicator) }
    function hideProgressBar () { navigationmanager.pop() }
    function setProgressText (str) { progressSubtitle = str; }
    function setProgress (fraction) { progressFraction = fraction; }
    signal progressCancelButton()

    // --------------------------- Midi player --------------------------------

    property bool playing: false
    property string midifilename: ""
    property double midiprogress: 0
    signal togglePlayPause()
    signal hearExample(int number)
    signal rewind()
    signal openFile (string filename)
    signal progressChangedManually (real percent)
    signal setTempoFactor (real factor)
    signal setRepeatMode (bool repeating)
    function setPlayingStatus (onoff) { playing = onoff }
    function setDisplayedMidiFilename (name) { midifilename = name }
    function setMidiProgress (percent) { midiprogress = percent }

    //------------------------- Temperament selector --------------------------

    readonly property variant initialWeights: [0.05,0.2,0.4,0.6,0.8,0.02,1,0.4,0.8,0.2,0.05,1.0]
    readonly property variant initialCents:   [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
    property variant customCents: initialCents
    property variant cents: initialCents
    property variant weights: initialWeights
    property bool subminorSeventh: false
    property int temperamentIndex: 1
    readonly property int customTemperamentIndex: 7
    property string customTemperamentName: ""
    function setTemperamentIndex (index) { temperamentIndex = index }
    function toggleJI () { temperamentIndex = (temperamentIndex>0 ? 0:1) }
    signal signalTuningMode (int tuningMode, int wolfsIntervalShift)
    signal signalCentsChanged   (int halftones, double value)
    signal signalWeightsChanged (int halftones, double value)
    signal signalSaveCustomTemperament (string t, string c, string w)
    signal signalLoadCustomTemperament ()
    function loadCustomTemperament(t,c,w) {
        customTemperamentName = t;
        cents = customCents = c.split(',').map(function(item) { return parseFloat(item, 10); });
        weights = w.split(',').map(function(item) { return parseFloat(item, 10); });
        console.log("Loaded custom temperament ",customTemperamentName,cents,weights);
        sendAllCentsAndWeights();
    }

    // Do not forget to set customTemperamentIndex: in Main.qml
    readonly property var temperamentNames:  [
        qsTr("Equal Temperament"),
        qsTr("Just Intonation"),
        qsTr("Pythagorean tuning"),
        qsTr("1/4 Meantone"),
        qsTr("Werckmeister III"),
        qsTr("Silbermann 1/6"),
        qsTr("Zarlino -2/7"),
        qsTr("User-defined")]

    function setTemperament (index) {
        // handle natural seventh in just intonation separately
        var sevnth = subminorSeventh ? -31.17 : 17.60
        var wolfsShift = 0
        switch(index)
        {
            //        halftone  majsec  minthrd majthrd fourth tritone fith  minsixth majsixth minsevn majsevn oct
            //           C#       D       D#      E       F      F#      G       G#       A      A#       B     C
            // EQUAL TEMPERAMENT
        case 0: cents = [0.0,   0.0,     0.0,     0.0,   0.0,    0.0,   0.0,     0.0,    0.0,    0.0,    0.0,   0.0]
            break;
            // JUST INTONATION
        case 1: cents = [11.73, 3.91,   15.64,  -13.69, -1.96,  -9.77,  1.96,   13.69,  -15.64,  sevnth, -11.73,  0.0]
            break;
            // PYTHAGOREAN TUNING
        case 2: cents = [-9.76,  3.91,   -5.87,  7.82,   -1.96,  11.73,  1.96,   -7.82,  5.87,   -3.91,  9.78,  0.0]
            wolfsShift = 10;
            break;
            // ONE QUARTER MEANTONE
        case 3: cents = [17.11, -6.83,  10.27,  -13.69, 3.42,   -20.53, -3.42,  13.69,  -10.26, 6.84,   -17.11, 0.0]
            wolfsShift = 10;
            break;
            // WERCKMEISTER III
        case 4: cents = [-3.91,	3.91,	0.00,	-3.91,	3.91,	0.00,	1.96,	-7.82,	0.00,	1.96,	-1.96,  0.0]
            wolfsShift = 0;
            break;
            // Silbermann 1/6
        case 5: cents = [-13.69,	-3.91,	5.87,	-7.82,	1.96,	-11.73,	-1.96,	-15.64,	-5.87,	3.91,	-9.78, 0.0]
            wolfsShift = 0;
            break;
            // Zarlino -2/7
        case 6: cents = [-29.33,	-8.38,	12.57,	-16.76,	4.19,	-25.14,	-4.19,	-33.52,	-12.57,	8.38,	-20.95, 0.0]
            wolfsShift = 0;
            break;
            // CUSTOM TUNING
        case customTemperamentIndex: cents = customCents;
            break;
            // DEFAULT: INITIAL VALUES
        default: cents = initialCents
            break;
        }
        signalTuningMode(Math.min(2,index), wolfsShift) // Tuning mode 0: ET, 1: JI, 2: custom
        sendAllCentsAndWeights()
    }

    function sendAllCentsAndWeights() {
        var i
        for (i=0; i<12; ++i) signalCentsChanged(i+1,cents[i])
        for (i=0; i<12; ++i) signalWeightsChanged(i+1,Math.exp(-3.0*(1-weights[i])))
    }

    //---------------------------- Tuning delay -------------------------------

    property double tuningDelay: 0
    signal setTuningDelay (double delay)
    onTuningDelayChanged: setTuningDelay(tuningDelay)

    //-------------------------------- Memory  --------------------------------

    property double memoryLength: 3
    signal setMemoryLength (double delay)
    onMemoryLengthChanged: setMemoryLength(memoryLength)

    //---------------------------- Static tuning ------------------------------

    property int staticTuningOffset: 0
    signal setStaticTuning (bool on, int index)

    //----------------------- Drift correction parameter ----------------------

    property double driftCorrectionParameter: 0.3
    signal setDriftCorrectionParameter (double lambda)
    signal resetPitchProgression()
    onDriftCorrectionParameterChanged: setDriftCorrectionParameter(driftCorrectionParameter)

    //---------------------------- Target frequency ---------------------------

    property double targetFrequency: 440
    readonly property double lowerTargetFrequency: 400
    readonly property double upperTargetFrequency: 480
    signal setFrequencyOfA4 (double frequency)
    function changeTargetFrequency (df) {
        if (targetFrequency+df >= lowerTargetFrequency &&
            targetFrequency+df <= upperTargetFrequency)
            targetFrequency += df
    }
    onTargetFrequencyChanged: setFrequencyOfA4(targetFrequency);

    //------------------ Harmonic progression circular gauge ------------------

    property double pitchDeviation: 0
    function setAveragePitchDeviation (deviation) { pitchDeviation = deviation }
    property double tension: 1
    function setTension (t) { tension = t }
    property string intervalString: ""
    function setIntervalString (str) { intervalString = str }

    //------------------ Downloading instruments from internet ----------------

    property bool allowDownload: false
    property bool downloading: false
    property int downloadedInstrument: 0
    readonly property int totalNumberOfInstruments: 3
    property int downloadingProgress: 0
    signal startDownload (bool forced)
    function showDownloader (show) {
        downloading = false;
        downloading = show;
        if (show) downloadedInstrument++;
    }
    function setDownloaderProgress (files,percent) { downloadingProgress = percent }
    function noInternetConnection (forced) {
        console.log("Qml: No internet connection availabel" , forced);
        navigationmanager.messageNoInternet()
    }

    //-------------------------- Midi input settings --------------------------

    property bool midiInputAutomaticMode: true
    property string selectedMidiInputDevice: ""
    property var availableMidiInputDevices: []
    signal onSelectedMidiInputDeviceNameChanged (string name)
    signal onAutomaticMidiInputModeChanged (bool on)

    function setQmlMidiInputDeviceNames (devices) {
        availableMidiInputDevices = devices;
        if (availableMidiInputDevices.length <= 2) selectedMidiInputDevice = qsTr("Inactive"); }
    function setQmlSelectedMidiInputDeviceName (name) { selectedMidiInputDevice = name; }

    //------------------------- Midi output settings --------------------------

    property bool midiOutputAutomaticMode: true
    property string selectedMidiOutputDevice: ""
    property var availableMidiOutputDevices: []
    property int externalMidiOutputChannel: 0
    signal onSelectedMidiOutputDeviceNameChanged (string name)
    signal onAutomaticMidiOutputModeChanged (bool on)
    signal onMidiOutputChannelChanged (int value)

    function setQmlMidiOutputDeviceNames (devices) {
        availableMidiOutputDevices = devices;
        if (availableMidiOutputDevices.length <= 2) selectedMidiOutputDevice = qsTr("Inactive"); }
    function setQmlSelectedMidiOutputDeviceName (name) { selectedMidiOutputDevice = name; }

    //------------------------- Audio output settings -------------------------

    property string currentAudioDevice: ""
    property var audioDeviceList: [ qsTr("[Choose Audio Device]") ]
    property int audioDeviceListIndex: 0
    property var sampleRateList: [ qsTr("[Choose]") ]

    property int currentAudioDeviceIndex: 0
    property int currentSampleRateIndex: 0
    property int currentAudioBufferSize: 0
    property int currentAudioPacketSize: 0

    property int selectedAudioDeviceIndex: currentAudioDeviceIndex
    property int selectedSampleRateIndex: currentSampleRateIndex
    property int selectedAudioBufferSize: currentAudioBufferSize
    property int selectedAudioPacketSize: currentAudioPacketSize

    function setAudioDeviceName (name) {
        currentAudioDevice = name;
        currentAudioDeviceIndex = audioDeviceList.indexOf(name)
    }
    function setAudioDeviceList (list,index) {
        audioDeviceList = list; audioDeviceListIndex = index
    }
    function setAudioSampleRates (list,index) {
        sampleRateList = list; currentSampleRateIndex = index
    }
    function setBufferSizes (buffer,packet)  {
        currentAudioBufferSize = -1;
        currentAudioPacketSize = -1;
        currentAudioBufferSize = buffer;
        currentAudioPacketSize = packet;
    }

    signal selectAudioParameters(int devIndex, int srIndex,
                                 int buffersize, int packetsize)

    //--------------------------- Touchpad keyboard ---------------------------

    signal sendTouchscreenKeyboardEvent (var keys);
    signal setToggleMode (bool on);
    signal clearTouchscreenKeyboard();
    property int highlightFlag: 0
    function highlightKey (key,on) { highlightFlag = 2*key + (on ? 1 : 0) }

    //--------------------------- File open dialog ----------------------------

    signal openFileOpenDialog (int width, int height);

    //======================== FINAL INITIALIZATION ===========================

    function getSettingsFromQml () {
        //if (showWelcome) playStartupSound()
        if (allowDownload) startDownload(false)
        setFrequencyOfA4 (targetFrequency)
        setDriftCorrectionParameter(driftCorrectionParameter)
        setMemoryLength(memoryLength)
        onMidiOutputChannelChanged(externalMidiOutputChannel)
        onAutomaticMidiInputModeChanged (midiInputAutomaticMode)
        onAutomaticMidiOutputModeChanged (midiOutputAutomaticMode)
        if (midiInputAutomaticMode) selectedMidiInputDevice=""
        else onSelectedMidiInputDeviceNameChanged (selectedMidiInputDevice)
        if (midiOutputAutomaticMode) selectedMidiOutputDevice=""
        else onSelectedMidiOutputDeviceNameChanged (selectedMidiOutputDevice)
    }

    //==========================================================================

    Rectangle {
        anchors.fill: parent
        color: "white"
    }

    Image {
        anchors.fill: parent
        source: "images/background.jpg"
        opacity: 0.3
    }

    NavigationManager {
        id: navigationmanager
        anchors.fill: parent
    }

    //------------------- Debugging message for mobiles -----------------------

    Text {
        id: errormsg
        visible: text != ""
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        text: ""
        color: "red"
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: parent.width*0.05
        anchors.bottomMargin: parent.height*0.18
        wrapMode: Text.WordWrap
    }

    function getMobileErrorMessage (str) {
        errormsg.text = str;
    }
}

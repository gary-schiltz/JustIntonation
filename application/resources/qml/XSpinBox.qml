/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4

SpinBox {
    id: spinBox
    property double fontsize: expert.fontsize
    implicitWidth: parent.width
    implicitHeight: window.scalefactor*0.06
    font.family: window.font
    font.pixelSize: fontsize
    horizontalAlignment: Qt.AlignHCenter

    style: SpinBoxStyle {

        background: Item {
            id: bg
            implicitWidth: control.width*1.3
            implicitHeight: control.height
            Rectangle {
                anchors.fill: parent
                anchors.margins: -parent.height*0.05
                radius: window.radius*1.1
                gradient: Gradient {
                    GradientStop { position: 0 ; color: "#fff" }
                    GradientStop { position: 1 ; color: "#000" }
                }
            }

            Rectangle {
                anchors.fill: parent
                color: "white"
                radius: window.radius
                border.color: "gray"
                border.width: 1
                gradient: Gradient {
                    GradientStop { position: 0 ; color: enabled ? "#eee" : "#fff" }
                    GradientStop { position: 1 ; color: enabled ? "#aaa" : "#ddd" }
                }
            }
        }

        incrementControl: Item {
            id: upcontrol
            implicitWidth: control.height
            implicitHeight: control.height
            Image {
                id: upArrow
                source: "icons/up.png"
                sourceSize.width: parent.width
                sourceSize.height: parent.height*0.8
                anchors.centerIn:parent
                opacity: control.value===control.maximumValue || !enabled  ? 0.2 : 1
            }
            MouseArea {
                id: mouse1
                anchors.fill:parent
            }
            Rectangle {
                color: "#11ffffff"
                anchors.fill: parent
                visible: mouse1.pressed
            }
        }
        decrementControl: Item {
            implicitWidth: control.height
            implicitHeight: control.height
            Image {
                id: downArrow
                source: "icons/down.png"
                sourceSize.width: parent.width
                sourceSize.height: parent.height*0.8
                anchors.centerIn: parent
                opacity: control.value===control.minimumValue  || !enabled ? 0.2 : 1
            }
            MouseArea {
                id: mouse2
                anchors.fill:parent
            }
            Rectangle {
                color: "#11ffffff"
                anchors.fill: parent
                visible: mouse2.pressed
            }
        }
    }
}


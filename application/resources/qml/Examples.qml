/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.4

Item {
    readonly property variant examplemodel: [
        "1: di Lasso: Sibylla Samia",
        "2: Allegri: Miserere mei",
        "3: Giaches de Wert: Adesto",
        "4: Cipriano de Rore: Calami",
        "5: Scheidemann: Praeambulum",
        "6: Bach: BWV 437",
        "7: Bach: BWV 723",
        "8: Alyabyev: Cherubic Hymn",
        "9: Brahms: Waltz No. 15",
        "10: Doonan: Enchanted Island",
        "11: Benedetti: Pitch up",
        "12: Benedetti: Pitch down"]
    readonly property variant exampleinfo: [
        "Orlando di Lasso (1532-1594): Sibylla Samia; Editor: Reinhold Schlötterer; License: CCSA 3.0; Link: http://www.mutopiaproject.org/ftp/LassusOd/samia/samia.mid",
        "Gregorio Allegri (1582-1652): Miserere mei, Deus; Editor: Bono Depoorter; License: CCSA 4.0; Link: http://www.mutopiaproject.org/ftp/AllegriG/miserere-mei-deus/miserere-mei-deus.mid",
        "Giaches de Wert (1524-1596): Adesto dolori meo; Editor: Ross W. Duffin; License: private permission",
        "Cipriano de Rore (1515-1565): Calami sonum ferentes; Editor: Gerd Eichler; License: CPDL; Link: http://www3.cpdl.org/wiki/images/9/94/Rore_Calami.mid",
        "H. Scheidemann (1595–1663): Praeambulum no.5 in d; Editor:  Tim Knigge; License: public domain; Link: http://www.mutopiaproject.org/ftp/ScheidemannH/HSpre5/HSpre5.mid",
        "J. S. Bach (1685–1750): Ich dank dir, lieber Herre; Editor: Laurent Ducos; License: public domain; Link: http://www.mutopiaproject.org/ftp/BachJS/BWV347/bwv347/bwv347.mid",
        "J. S. Bach (1685–1750): : Gelobet seist du, Jesu Christ; Editor: Urs Metzger; License: public domain; Link: http://www.mutopiaproject.org/ftp/BachJS/BWV723/bwv723/bwv723.mid",
        "A. Alyabyev (1787–1851): Cherubic Hymn; Editor: Daniel Johnson; License: public domain: Link: http://www.mutopiaproject.org/ftp/AlyabyevA/alyabyev-cherubic/alyabyev-cherubic.mid",
        "Johannes Brahms (1833-1897): Waltz No. 15; Editor: R. Joseffy; License: public domain; Link: http://www.mutopiaproject.org/ftp/BrahmsJ/O39/waltz-op39-15/waltz-op39-15.mid",
        "Stephen Charles Doonan (1952-): Enchanted Island; License: CCSA 2.5; Link: http://www.mutopiaproject.org/ftp/DoonanSC/enchanted-island/enchanted-island.mid",
        "Giovanni Battista Benedetti (1539-1590): Loop for upward pitch progression; Editor: Ross W. Duffin; License: private permission",
        "Giovanni Battista Benedetti (1539-1590): Loop for downward pitch progression; Editor: Ross W. Duffin; License: private permission"]

    Rectangle {
        id: infobox
        anchors.fill: parent
        radius: parent.width*0.02
        z: 10
        visible: false
        XText {
            id: infotext
            wrapMode: Text.Wrap
            anchors.fill: parent
            anchors.margins: parent.width*0.02
            anchors.topMargin: parent.width*0.04
            font.pixelSize: expert.fontsize*0.7
            text: "<b>"+qsTr("Example ") + (window.exampleindex+1) + ": </b><br>" +
                  exampleinfo[window.exampleindex]
        }
        Image {
            source: "qrc:/icons/cancel.png"
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.margins: fontsize*0.2
            width: fontsize*1.2
            height: width
            asynchronous: true
            smooth: true
            mipmap: true
        }

        MouseArea {
            z:11
            anchors.fill: parent
            enabled: infobox.visible
            onClicked: infobox.visible = false
        }
    }

    Column {
        anchors.fill: parent
        anchors.leftMargin: parent.width*0.02
        anchors.rightMargin: parent.height*0.09
        anchors.topMargin: parent.height*0.09
        spacing: parent.height*0.09
        Item {
            id: firstrow
            width: parent.width
            height: width*0.13
            XButton
            {
                anchors.fill: parent
                anchors.rightMargin: parent.width*0.8
                text: "←"
                fontsize: expert.fontsize*1.2
                onClicked: browseExamples(-1)
                tooltip: qsTr("Play previous MIDI example")
            }
            XButton
            {
                anchors.fill: parent
                anchors.leftMargin: parent.width*0.25
                anchors.rightMargin: parent.width*0.25
                text: "© " + qsTr("Info")
                fontsize: expert.fontsize
                onClicked: infobox.visible = true
                tooltip: qsTr("Show copyright information about the MIDI file")
            }
            XButton
            {
                anchors.fill: parent
                anchors.leftMargin: parent.width*0.8
                text: "→"
                fontsize: expert.fontsize
                onClicked: browseExamples(1)
                tooltip: qsTr("Play next MIDI example")
            }
        }
        XCombo {
            id: exampleselector
            width: parent.width
            height: width*0.13
            model: examplemodel
            property int index: window.exampleindex
            currentIndex: index
            fontsize: expert.fontsize
            onCurrentIndexChanged: {
                if (currentIndex != window.exampleindex) {
                    window.setQmlSelectedExample(currentIndex)
                    window.hearExample(currentIndex+1)
                }
            }
            onIndexChanged: currentIndex = window.exampleindex
        }

    }
    Component.onCompleted: console.assert(examplemodel.length==numberOfExamples,
               "ERROR: Number of examples defined in Main.qml does not match");

}

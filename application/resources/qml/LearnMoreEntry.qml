/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3

// Entry for the "Learn more"-page, including mouse area

Item
{
    id: learnmoreentry

    height: learnmore.iconheight
    width: parent.width
    property alias source: icon.source
    property alias text: text.text
    property alias tooltip: icon.tooltip

    signal clicked()

    MouseArea {
        anchors.fill: parent
        onClicked: learnmoreentry.clicked()
    }

    XToolButton {
        id: icon
        height: parent.height
        width: parent.height
        onClicked: learnmoreentry.clicked()
    }
    XText {
        id: text
        height: parent.height
        x: parent.height * 1.2
        width: parent.width*0.9-x
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: window.fontsize*0.7 + window.height*0.01
    }
}


/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#include <QtGlobal>

//=============================================================================
//              Compile-time configuration of the application
//=============================================================================

#ifndef CONFIG_H
#define CONFIG_H

//------------------------- Application name etc ------------------------------

#define INT_APPLICATIONNAME "Just Intonation"
#define INT_ORGANIZATIONNAME "University of Wuerzburg"
#define INT_ORGANIZATIONDOMAIN "justintonation.tp3.app"
#define INT_APPLICATIONNAME_LOWERCASE "justintonation"

//----------------------------- Version numbers -------------------------------

// To modify the version number update here and then call script upateVersion.h
#define INT_APPLICATION_VERSION "1.3.2"
#define INT_ROLLING_APPLICATION_VERSION 22
#define INT_FILEFORMAT_ROLLING_VERSION 2

//------------------------------ Internet urls --------------------------------

#define INT_SAMPLES_REPOSITORY "http://samples.just-intonation.org"

//----------------------------- Midi examples ---------------------------------

#define INT_NUMBER_OF_EXAMPLES 12

//----------------------- Platform-specific settings --------------------------

// INT_BUFFERSIZE is the default buffer size of the internal Qt-Audio buffer
// Use 0 if no setting
// INT_MAXFRAMES is the maximal size

// ANDROID
#if defined(Q_OS_ANDROID)
#define INT_BUFFERSIZE 0
#define INT_PACKETSIZE 32
#define INT_TEMPERAMENT_DIR QStandardPaths::AppLocalDataLocation

// Stable Linux settins (empirical)
#elif defined(Q_OS_LINUX)
#define INT_BUFFERSIZE 16384
#define INT_PACKETSIZE 1024
#define INT_TEMPERAMENT_DIR QStandardPaths::DocumentsLocation

// MAC OSX
#elif (defined(Q_OS_DARWIN) && !defined(Q_OS_IOS))
#define INT_BUFFERSIZE 0
#define INT_PACKETSIZE 256
#define INT_TEMPERAMENT_DIR QStandardPaths::DocumentsLocation

// IOS
#elif defined(Q_OS_IOS)
#define INT_BUFFERSIZE 0
#define INT_PACKETSIZE 256
#define INT_TEMPERAMENT_DIR QStandardPaths::AppLocalDataLocation

// WINDOWS DESKTOP
#elif defined(Q_OS_WIN64)
#define INT_BUFFERSIZE 16384
#define INT_PACKETSIZE 1024
#define INT_TEMPERAMENT_DIR QStandardPaths::DocumentsLocation

// WINDOWS UWP
#elif defined(Q_OS_WINRT)
#define INT_BUFFERSIZE 1024
#define INT_PACKETSIZE 1024
#define INT_TEMPERAMENT_DIR QStandardPaths::AppLocalDataLocation

// Generic default values
#else
#define INT_BUFFERSIZE 1024
#define INT_PACKETSIZE 1024
#define INT_TEMPERAMENT_DIR QStandardPaths::DocumentsLocation
#endif

//-------------------------------- RunGuard -----------------------------------

#if (!defined(Q_OS_ANDROID) && !defined(Q_OS_IOS))
#define INT_INCLUDE_RUNGUARD
#endif


//=============================================================================
//             Doxygen comments describing the groups of classes
//=============================================================================

//-----------------------------------------------------------------------------
// SYSTEM:
/// \defgroup system System components
/// \brief Collection of various system-related classes.
/// \details This group comprises various classes which are related to
/// generic system properties.
//-----------------------------------------------------------------------------
// INSTRUMENT:
/// \defgroup instrument Instrument module
/// \brief Components holding the static data of the musical instrument,
/// in particular the samples.
/// \details This group comprises all classes which hold the static data of
/// the instrument.
//-----------------------------------------------------------------------------
// SOUND-GENERATING
/// \defgroup soundgenerator Sound-generating module
/// \brief Classes dealing with the dynamical generation of sampled sounds.
//-----------------------------------------------------------------------------
// IMPORTER
/// \defgroup importer Import module
/// \brief Component for importing recorded samples.
//-----------------------------------------------------------------------------


#endif // CONFIG_H

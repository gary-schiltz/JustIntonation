/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#ifndef INSTRUMENTFILEHANDLER_H
#define INSTRUMENTFILEHANDLER_H

#include <QIODevice>

///////////////////////////////////////////////////////////////////////////////
/// \brief Class for reading and writing an entire instrument
/// \details This helper class allows us to write and read objects of
/// different type in binary format.
///////////////////////////////////////////////////////////////////////////////

class InstrumentFileHandler
{
public:
    InstrumentFileHandler();

    template <class T>
    static bool write(QIODevice &iodevice, const T &object);

    template <class T>
    static bool read(QIODevice &iodevice, T &object);
};

#endif // INSTRUMENTFILEHANDLER_H

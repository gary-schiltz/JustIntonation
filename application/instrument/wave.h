/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//            Wave: Class holding the sampled sound for one key
//=============================================================================

#ifndef WAVE_H
#define WAVE_H

#include <QVector>
#include <QIODevice>
#include <complex>

///////////////////////////////////////////////////////////////////////////////
/// \brief Class holding the sampled sound for one key
/// \ingroup instrument
/// \details A sample is essentially an array of N left-right stereo pairs
/// of signed 16-bit PCM values. This class contains two samples, namely,
/// a long mSustainSample (up to 23.8 sec) for the sound of the instrument
/// while the key is pressed, and a short mReleaseSample for the sound of
/// the instrument in the moment when the key is released (usually about
/// a few seconds).
///
/// The sustain sample is limited by the maxNumberOfFrames which is defined
/// here as 20 bits or 1048576 frames. With a standard sample rate of
/// 44100Hz this corresponds to a duration of 23.77 sec. Thus the maximum
/// size of a sample is about 4MB. 88 keys would make up about 350MB of
/// sample data.
/// \see Scale, Voice, Instrument
///////////////////////////////////////////////////////////////////////////////

class Wave
{
public:
    Wave();

    //friend class Sampler;

    bool read (QIODevice &iodevice);
    bool write (QIODevice &iodevice);

    bool insert (const QVector<qint32> &L, const QVector<qint32> &R,
                 const bool release, const double amplitude);

    void printInfo (int keynumber=0);

    quint32 getRepetitionIndex()                { return mRepetitionIndex; }
    double getRepetitionFactor()                { return mRepetitionFactor; }
    const QVector<qint16>* getSustainSample()   { return &mSustainSample; }
    const QVector<qint16>* getReleaseSample()   { return &mReleaseSample; }
    int getReleaseShift()                       { return mReleaseShift; }
    int getSustainShift()                       { return mSustainShift; }

    double getEnvelope (int index) { return mEnvelope[index/envelopeWidth]; }

    bool envelopeExists () { return mEnvelope.size() > 0; }
    bool waveFormExists () { return mSustainSample.size() > 0; }
    bool releaseExists ()  { return mReleaseSample.size() > 0; }

    void computeTriangularWave(double frequency, int samplerate, double stereo);
private:
    quint32 mRepetitionIndex;           ///< Index from where on the sample is repeated
    double mRepetitionFactor;           ///< Amplitude decrease factor upon repetition
    QVector<qint16> mSustainSample;     ///< Sound sample when key is pressed
    QVector<qint16> mReleaseSample;     ///< Sound sample when key is releasek
    int mSustainShift;                  ///< PCM amplitudes shifted # bits to the left
    int mReleaseShift;                  ///< PCM amplitedes shifted # bits to the left
    QVector<double> mEnvelope;          ///< Amplitude envelope

    void computeEnvelope(int samplerate);
    void automaticCyclicMorphing();     ///< Function defined externally (Importer)

private:
    const int maxNumberOfFrames = (1<<20)-2;    ///< Maximal number of frames
    const int envelopeWidth = 0x1000;           ///< Frames per envelope point
};

#endif // WAVE_H
